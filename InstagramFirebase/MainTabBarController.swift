//
//  MainTabBarController.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-02-07.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Firebase

class MainTabBarController: UITabBarController {
  override func viewDidLoad() {
    super.viewDidLoad()
    self.delegate = self

    setupAppearance()

    if Auth.auth().currentUser == nil {
      DispatchQueue.main.async {
        let loginController = LoginController()
        let navController = UINavigationController(rootViewController: loginController)
        self.present(navController, animated: true, completion: nil)
      }
      return
    }

    setupViewControllers()
  }

  public func setupViewControllers() {
    let homeNavController = templateNavController(unselectedImage: #imageLiteral(resourceName: "home_unselected"), selectedImage: #imageLiteral(resourceName: "home_selected"), rootViewController: HomeController(collectionViewLayout: UICollectionViewFlowLayout()))

    let searchNavController = templateNavController(unselectedImage: #imageLiteral(resourceName: "search_unselected"), selectedImage: #imageLiteral(resourceName: "search_unselected"), rootViewController: UserSearchController(collectionViewLayout: UICollectionViewFlowLayout()))

    let plusNavController = templateNavController(unselectedImage: #imageLiteral(resourceName: "plus_unselected"), selectedImage: #imageLiteral(resourceName: "plus_unselected"))

    let likeNavController = templateNavController(unselectedImage: #imageLiteral(resourceName: "like_unselected"), selectedImage: #imageLiteral(resourceName: "like_selected"))

    let layout = UICollectionViewFlowLayout()
    let userProfileController = UserProfileController(collectionViewLayout: layout)

    let userProfileNavController = templateNavController(unselectedImage: #imageLiteral(resourceName: "profile_unselected"), selectedImage: #imageLiteral(resourceName: "profile_selected"), rootViewController: userProfileController)

    viewControllers = [homeNavController, searchNavController, plusNavController, likeNavController, userProfileNavController]

    guard let items = tabBar.items else { return }
    items.forEach { (item) in
      item.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
    }

  }
}

// MARK: UITabBarControllerDelegate
extension MainTabBarController: UITabBarControllerDelegate {
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    let index = viewControllers?.index(of: viewController)

    // 0 based index, if the Photo screen is selected
    if index == 2 {
      let layout = UICollectionViewFlowLayout()
//      layout.scrollDirection = .horizontal
      let photoSelectorController = PhotoSelectorController(collectionViewLayout: layout)
      let navController = UINavigationController(rootViewController: photoSelectorController)

      present(navController, animated: true, completion: nil)

      return false
    }
    
    return true
  }
}


// MARK: Setup
private extension MainTabBarController {
  func setupAppearance() {
    tabBar.tintColor = .black
  }

  func templateNavController(unselectedImage: UIImage, selectedImage: UIImage, rootViewController: UIViewController = UIViewController()) -> UINavigationController {
    let viewController = rootViewController
    let navController = UINavigationController(rootViewController: viewController)
    navController.tabBarItem.image = unselectedImage
    navController.tabBarItem.selectedImage = selectedImage
    return navController
  }
}
