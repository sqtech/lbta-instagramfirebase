//
//  LoginController.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-02-18.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Firebase

class LoginController: UIViewController {

  let logoContainerView: UIView = {
    let view = UIView()
    let logoImageView = UIImageView(image: #imageLiteral(resourceName: "Instagram_logo_white"))
    logoImageView.contentMode = .scaleAspectFill

    view.addSubview(logoImageView)
    logoImageView.anchor(top: nil, leading: nil, bottom: nil, trailing: nil, size: .init(width: 200, height: 50))
    logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    logoImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

    view.backgroundColor = UIColor.rgb(red: 0, green: 120, blue: 175)
    return view
  }()

  let emailTextField: UITextField = {
    let tf = UITextField()
    tf.placeholder = "Email"
    tf.backgroundColor = UIColor(white: 0, alpha: 0.05)
    tf.borderStyle = .roundedRect
    tf.font = UIFont.systemFont(ofSize: 14)

    tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
    return tf
  }()

  let passwordTextField: UITextField = {
    let tf = UITextField()
    tf.placeholder = "Password"
    tf.isSecureTextEntry = true
    tf.backgroundColor = UIColor(white: 0, alpha: 0.05)
    tf.borderStyle = .roundedRect
    tf.font = UIFont.systemFont(ofSize: 14)

    tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
    return tf
  }()

  let loginButton: UIButton = {
    let button = UIButton(type: .system)
    button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
    button.setTitle("Login", for: .normal)
    button.setTitleColor(.white, for: .normal)
    button.backgroundColor = UIColor.inactiveLightBlue
    button.layer.cornerRadius = 5
    button.isEnabled = false

    button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
    return button
  }()

  let dontHaveAccountButton: UIButton = {
    let button = UIButton(type: .system)
    let attributedTitle = NSMutableAttributedString(string: "Don't have an account?  ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor.lightGray])
    attributedTitle.append(NSAttributedString(string: "Sign Up", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor.lightBlue]))
    button.setAttributedTitle(attributedTitle, for: .normal)
//    button.setTitle("Don't have an account? Sign Up.", for: .normal)
    button.addTarget(self, action: #selector(handleDontHaveAccount), for: .touchUpInside)
    return button
  }()

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
  }
}

private extension LoginController {

  @objc func handleTextInputChange() {
    guard let email = emailTextField.text, let password = passwordTextField.text else { return }

    let isFormValid = !email.isEmpty && !password.isEmpty

    if isFormValid {
      loginButton.isEnabled = true
      loginButton.backgroundColor = UIColor.lightBlue
    } else {
      loginButton.isEnabled = false
      loginButton.backgroundColor = UIColor.inactiveLightBlue
    }
  }

  @objc func handleLogin() {
    print("handleLogin")
    guard let email = emailTextField.text, let password = passwordTextField.text else { return }

    if !email.isEmpty && !password.isEmpty {
      Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
        if let error = error {
          print("Failed to sign in with email:", error)
          return
        }

        print("Successfully logged in with user:", user?.uid ?? "")

        guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else { return }
        mainTabBarController.setupViewControllers()
        self.dismiss(animated: true, completion: nil)

      })
    }
  }

  func setupLayout() {
    navigationController?.isNavigationBarHidden = true
    view.backgroundColor = .white

    view.addSubview(logoContainerView)
    logoContainerView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, size: .init(width: 0, height: 150))

    setupInputFields()

    view.addSubview(dontHaveAccountButton)
    dontHaveAccountButton.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 50))
  }

  func setupInputFields() {
    let stackView = UIStackView(arrangedSubviews: [emailTextField, passwordTextField, loginButton])
    stackView.axis = .vertical
    stackView.distribution = .fillEqually
    stackView.spacing = 10

    view.addSubview(stackView)
    stackView.anchor(top: logoContainerView.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 20, left: 40, bottom: 0, right: 40), size: .init(width: 0, height: 140))
  }

  @objc func handleDontHaveAccount() {
    let signUpController = SignUpController()
    navigationController?.pushViewController(signUpController, animated: true)
  }
}
