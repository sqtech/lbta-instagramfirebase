//
//  SignUpController.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-01-23.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Firebase

class SignUpController: UIViewController {

  let plusPhotoButton: UIButton = {
    let button = UIButton(type: .system)
    button.setImage(#imageLiteral(resourceName: "plus_photo"), for: .normal)
//    button.setImage(#imageLiteral(resourceName: "plus_photo").withRenderingMode(UIImageRenderingMode.alwaysOriginal), for: .normal)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.addTarget(self, action: #selector(handlePlusPhoto), for: .touchUpInside)
    return button
  }()

  let emailTextField: UITextField = {
    let tf = UITextField()
    // Don't need to set this anymore if calling the anchor method
    // Removed in below text fields
    tf.translatesAutoresizingMaskIntoConstraints = false
    tf.placeholder = "Email"
    tf.leftViewMode = UITextFieldViewMode.always
    tf.leftView = UIImageView(image: #imageLiteral(resourceName: "like_unselected"))
    tf.backgroundColor = UIColor(white: 0, alpha: 0.05)
    tf.borderStyle = .roundedRect
    tf.font = UIFont.systemFont(ofSize: 14)

    tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
    return tf
  }()

  let usernameTextField: UITextField = {
    let tf = UITextField()
    tf.placeholder = "User Name"
    tf.backgroundColor = UIColor(white: 0, alpha: 0.05)
    tf.borderStyle = .roundedRect
    tf.font = UIFont.systemFont(ofSize: 14)

    tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
    return tf
  }()

  let passwordTextField: UITextField = {
    let tf = UITextField()
    tf.placeholder = "Password"
    tf.isSecureTextEntry = true
    tf.backgroundColor = UIColor(white: 0, alpha: 0.05)
    tf.borderStyle = .roundedRect
    tf.font = UIFont.systemFont(ofSize: 14)

    tf.addTarget(self, action: #selector(handleTextInputChange), for: .editingChanged)
    return tf
  }()

  let signupButton: UIButton = {
    let button = UIButton(type: .system)
    button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
    button.setTitle("Sign Up", for: .normal)
    button.setTitleColor(.white, for: .normal)
    button.backgroundColor = UIColor.inactiveLightBlue
    button.layer.cornerRadius = 5
    button.isEnabled = false

    button.addTarget(self, action: #selector(handleSignup), for: .touchUpInside)
    return button
  }()

  let alreadyHaveAccountButton: UIButton = {
    let button = UIButton(type: .system)
    let attributedTitle = NSMutableAttributedString(string: "Have an account?  ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor.lightGray])
    attributedTitle.append(NSAttributedString(string: "Log In", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor.lightBlue]))
    button.setAttributedTitle(attributedTitle, for: .normal)
    button.addTarget(self, action: #selector(handleAlreadyHaveAccount), for: .touchUpInside)
    return button
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
  }

}

private extension SignUpController {
  // MARK: Handlers
  @objc func handleTextInputChange() {
    guard let username = usernameTextField.text, let email = emailTextField.text, let password = passwordTextField.text else { return }

    let isFormValid = !email.isEmpty && !username.isEmpty && !password.isEmpty

    if isFormValid {
      signupButton.isEnabled = true
      signupButton.backgroundColor = UIColor.lightBlue
    } else {
      signupButton.isEnabled = false
      signupButton.backgroundColor = UIColor.inactiveLightBlue
    }
  }

  @objc func handleSignup() {
    guard let username = usernameTextField.text, !username.isEmpty else { return }
    guard let email = emailTextField.text, !email.isEmpty else { return }
    guard let password = passwordTextField.text, !password.isEmpty else { return }

    Auth.auth().createUser(withEmail: email, password: password) { (user: User?, error: Error?) in
      if let error = error {
        print("Failed to create user:", error)
        return
      }

      guard let uid = user?.uid else { return }
      guard let isOriginalImage = self.plusPhotoButton.imageView?.image?.isEqualToImage(image: #imageLiteral(resourceName: "plus_photo")) else { return }

      // Save the user with profile image
      // User has changed image if not original image
      if !isOriginalImage, let image = self.plusPhotoButton.imageView?.image, let uploadData = UIImageJPEGRepresentation(image, 0.3) {
        let filename = UUID().uuidString
        Storage.storage().reference().child("profile_images").child(filename).putData(uploadData, metadata: nil, completion: { (metaData, error) in
          if let error = error {
            print("Failed to upload profile image:", error)
            return
          }
          guard let profileImageURL = metaData?.downloadURL()?.absoluteString else { return }

          self.saveUser(uid: uid, with: username, imageURL: profileImageURL)

//          print("Successfully uploaded profile image:", profileImageURL)
        })
      } else {
        self.saveUser(uid: uid, with: username)
      }

    }
  }

  @objc func handleAlreadyHaveAccount() {
    navigationController?.popViewController(animated: true)
  }

  @objc func handlePlusPhoto() {
    let imagePickerController = UIImagePickerController()
    imagePickerController.delegate = self
    imagePickerController.allowsEditing = true
    present(imagePickerController, animated: true, completion: nil)
    print("handlePlusPhoto")
  }


  // MARK: Methods
  func saveUser(uid: String, with username: String, imageURL: String = "") {
    let dictionaryValues = ["username": username, "profileImageURL": imageURL]
    let values = [uid: dictionaryValues]

    Database.database().reference().child("users").updateChildValues(values) { (error, ref) in
      if let error = error {
        print ("Failed to save user info into db:", error)
        return
      }

      guard let mainTabBarController = UIApplication.shared.keyWindow?.rootViewController as? MainTabBarController else { return }
      mainTabBarController.setupViewControllers()
      self.dismiss(animated: true, completion: nil)
//      print("Successfully saved user info to db")
    }
  }
  func setupLayout() {
    view.backgroundColor = .white
    view.addSubview(plusPhotoButton)
    plusPhotoButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 40, left: 0, bottom: 0, right: 0), size: .init(width: 140, height: 140))
    plusPhotoButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

    view.addSubview(alreadyHaveAccountButton)
    alreadyHaveAccountButton.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 50))

    setupInputFields()

  }
  func setupInputFields() {
    let stackView = UIStackView(arrangedSubviews: [emailTextField, usernameTextField, passwordTextField, signupButton])
    stackView.axis = .vertical
    stackView.distribution = .fillEqually
    stackView.spacing = 10
    // Don't need to set this anymore if calling the anchor method
    //    stackView.translatesAutoresizingMaskIntoConstraints = false

    view.addSubview(stackView)
    stackView.anchor(top: plusPhotoButton.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 20, left: 40, bottom: 0, right: 40), size: .init(width: 0, height: 200))

    // Playing with constraints
//    let widthConstraint: NSLayoutConstraint = NSLayoutConstraint(item: emailTextField, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.lessThanOrEqual, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 20)
//    widthConstraint.priority = UILayoutPriority(rawValue: 999)

//    var someAnchor: NSLayoutConstraint = emailTextField.widthAnchor.constraint(equalToConstant: 200)
//    someAnchor.isActive = true
//    emailTextField.widthAnchor.constraint(lessThanOrEqualToConstant: 20).priority = UILayoutPriority(rawValue: 999)

  }
}

// MARK: UIImagePickerControllerDelegate
extension SignUpController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    dismiss(animated: true, completion: nil)
  }

  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    // https://developer.apple.com/documentation/uikit/uiimagepickercontrollerdelegate for keys

    if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
      plusPhotoButton.setImage(editedImage.withRenderingMode(.alwaysOriginal), for: .normal)
    } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
      plusPhotoButton.setImage(originalImage.withRenderingMode(.alwaysOriginal), for: .normal)
    }

    plusPhotoButton.circle()
    dismiss(animated: true, completion: nil)

  }
}
