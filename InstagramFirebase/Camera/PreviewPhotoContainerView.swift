//
//  PreviewPhotoContainerView.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-05-06.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Photos

class PreviewPhotoContainerView: UIView {
  let previewImageView: UIImageView = {
    let iv = UIImageView()
    iv.contentMode = .scaleAspectFill
    return iv
  }()

  let cancelButton: UIButton = {
    let button = UIButton(type: .system)
    button.setImage(#imageLiteral(resourceName: "cancel_shadow").withRenderingMode(.alwaysOriginal), for: .normal)
    button.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
    return button
  }()

  let saveButton: UIButton = {
    let button = UIButton(type: .system)
    button.setImage(#imageLiteral(resourceName: "save_shadow").withRenderingMode(.alwaysOriginal), for: .normal)
    button.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
    return button
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setupLayout()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

private extension PreviewPhotoContainerView {
  func setupLayout() {
    addSubview(previewImageView)
    previewImageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)

    addSubview(cancelButton)
    cancelButton.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 12, left: 12, bottom: 0, right: 0), size: .init(width: 25, height: 25))

    addSubview(saveButton)
    saveButton.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 0, left: 24, bottom: 24, right: 0), size: .init(width: 45, height: 50))
  }

  // MARK: Handlers
  @objc func handleSave() {
    print("handleSave")

    guard let previewImage = previewImageView.image else { return }

    let library = PHPhotoLibrary.shared()
    library.performChanges({
      PHAssetChangeRequest.creationRequestForAsset(from: previewImage)
    }) { (success, err) in
      if let err = err {
        print("Failed to save image to photo library:", err)
        return
      }

      print("Successfully saved image to library")

      // UI Changes are always on main thread
      DispatchQueue.main.async {
        let savedLabel = UILabel()
        savedLabel.font = UIFont.boldSystemFont(ofSize: 18)
        savedLabel.text = "Saved Successfully"
        savedLabel.textColor = .white
        savedLabel.textAlignment = .center
        savedLabel.backgroundColor = UIColor(white: 0, alpha: 0.3)
        savedLabel.numberOfLines = 0
        savedLabel.frame = CGRect(x: 0, y: 0, width: 150, height: 80)
        savedLabel.center = self.center
        
        self.addSubview(savedLabel)
        savedLabel.layer.transform = CATransform3DMakeScale(0, 0, 0)

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
          savedLabel.layer.transform = CATransform3DMakeScale(1, 1, 1)
        }, completion: { completed in
          // Once completed, execute another animation
          UIView.animate(withDuration: 0.5, delay: 0.75, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            savedLabel.layer.transform = CATransform3DMakeScale(0.1, 0.1, 0.1)
            savedLabel.alpha = 0
          }, completion: { completed in
            savedLabel.removeFromSuperview()
          })
        })
      }
    }
  }

  @objc func handleCancel() {
    self.removeFromSuperview()
  }
}
