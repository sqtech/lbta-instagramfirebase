//
//  CameraController.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-05-02.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import AVFoundation

class CameraController: UIViewController {
  let capturePhotoButton: UIButton = {
    let button = UIButton(type: .system)
    button.setImage(#imageLiteral(resourceName: "capture_photo").withRenderingMode(.alwaysOriginal), for: .normal)
    button.addTarget(self, action: #selector(handleCapturePhoto), for: .touchUpInside)
    return button
  }()

  let dismissButton: UIButton = {
    let button = UIButton(type: .system)
    button.setImage(#imageLiteral(resourceName: "right_arrow_shadow").withRenderingMode(.alwaysOriginal), for: .normal)
    button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
    return button
  }()

  let customAnimationPresentor = CustomAnimationPresentor()
  let customAnimationDismisser = CustomAnimationDismisser()

  // MARK: AVCapture Property
  let output = AVCapturePhotoOutput()

  override func viewDidLoad() {
    super.viewDidLoad()

    transitioningDelegate = self
    setupCaptureSession()
    setupLayout()
  }

  override var prefersStatusBarHidden: Bool {
    return true
  }
}

private extension CameraController {
  func setupLayout() {
    view.addSubview(capturePhotoButton)
    capturePhotoButton.anchor(top: nil, leading: nil, bottom: view.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: 24, right: 0), size: .init(width: 100, height: 100))
    capturePhotoButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

    view.addSubview(dismissButton)
    dismissButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 12, left: 0, bottom: 0, right: 12), size: .init(width: 35, height: 35))
  }

  func setupCaptureSession() {
    let captureSession = AVCaptureSession()

    // 1. Setup input
    guard let captureDevice = AVCaptureDevice.default(for: .video) else {
      print("Can not get default device")
      return
    }

    do {
      let input = try AVCaptureDeviceInput(device: captureDevice)
      if captureSession.canAddInput(input) {
        captureSession.addInput(input)
      }

    } catch {
      print("Could not setup camera input", error)
    }

    // 2. Setup outputs

    if captureSession.canAddOutput(output) {
      captureSession.addOutput(output)
    }

    // 3. Setup output preview
    let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
    previewLayer.frame = view.frame

    view.layer.addSublayer(previewLayer)

    captureSession.startRunning()
  }

  @objc func handleCapturePhoto() {
    print("handleCapturePhoto")
    let settings = AVCapturePhotoSettings()
    guard let formatType = settings.availablePreviewPhotoPixelFormatTypes.first else { return }
    settings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: formatType]
    output.capturePhoto(with: settings, delegate: self)
  }

  @objc func handleDismiss() {
    dismiss(animated: true, completion: nil)
  }

}

// MARK: UIViewControllerTransitioningDelegate
extension CameraController: UIViewControllerTransitioningDelegate {
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return customAnimationPresentor
  }

  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return customAnimationDismisser
  }
}

// MARK: AVCapturePhotoCaptureDelegate
extension CameraController: AVCapturePhotoCaptureDelegate {
  func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
    print("Finish processing photo sample buffer...")
    guard let imageData = photo.fileDataRepresentation() else { return }
    let previewImage = UIImage(data: imageData)

    let containerView = PreviewPhotoContainerView()
    containerView.previewImageView.image = previewImage
    view.addSubview(containerView)
    containerView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)

//    let previewImageView = UIImageView(image: previewImage)
//    previewImageView.contentMode = .scaleAspectFill
//    view.addSubview(previewImageView)
//    previewImageView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
  }

//  func photoOutput(_ output: AVCapturePhotoOutput, didFinishCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings, error: Error?) {
//    <#code#>
//  }
}
