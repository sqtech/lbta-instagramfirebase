//
//  CustomAnimationPresentor.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-05-06.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

/// For Animating from HomeController to CameraController
class CustomAnimationPresentor: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.5
  }

  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView

    // Will be the HomeController
    guard let fromView = transitionContext.view(forKey: .from) else { return }

    // Will be the CameraController
    guard let toView = transitionContext.view(forKey: .to) else { return }
    containerView.addSubview(toView)

    // Left most side
    let startingFrame = CGRect(x: -toView.frame.width, y: 0, width: toView.frame.width, height: toView.frame.height)

    toView.frame = startingFrame

    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
      fromView.frame = CGRect(x: fromView.frame.width, y: 0, width: fromView.frame.width, height: fromView.frame.height)
      toView.frame = CGRect(x: 0, y: 0, width: toView.frame.width, height: toView.frame.height)
    }) { _ in
      // Allows added view to be dismissed/removed
      transitionContext.completeTransition(true)
    }

  }
}
