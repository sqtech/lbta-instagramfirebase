//
//  UserProfileController.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-02-07.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Firebase

class UserProfileController: UICollectionViewController {
  struct CellIdentifiers {
    static let headerId = "HeaderId"
    static let photoCellId = "CellId"
    static let homePostCellId = "HomePostCellId"
  }

  var user: AppUser?
  var posts = [Post]()
  var isGridView = true

  // passed from SearchController
  var userId: String?

  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView?.backgroundColor = .white
    collectionView?.register(UserProfileHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: CellIdentifiers.headerId)
    collectionView?.register(UserProfilePhotoCell.self, forCellWithReuseIdentifier: CellIdentifiers.photoCellId)
    collectionView?.register(HomePostCell.self, forCellWithReuseIdentifier: CellIdentifiers.homePostCellId)
    fetchUser()
//    fetchPosts()
    setupLogoutButton()
  }

  override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CellIdentifiers.headerId, for: indexPath) as! UserProfileHeader

    header.delegate = self
    header.user = user
    return header
  }

  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return posts.count
  }

  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if isGridView {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.photoCellId, for: indexPath) as! UserProfilePhotoCell
      cell.post = posts[indexPath.item]
      return cell
    } else {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.homePostCellId, for: indexPath) as! HomePostCell
      cell.post = posts[indexPath.item]
      return cell
    }
  }

}

// MARK: Firebase API Calls
private extension UserProfileController {
  func fetchOrderedPosts() {
    guard let uid = self.user?.uid else { return }

    let ref = Database.database().reference(withPath: "posts").child(uid)
    ref.queryOrdered(byChild: "creationDate").observe(.childAdded, with: { (snapshot) in
//      print(snapshot.key)
      guard let dictionary = snapshot.value as? [String: Any], let user = self.user else { return }

      let post = Post(user: user, dictionary: dictionary)
      self.posts.insert(post, at: 0)
//      self.posts.append(post)
      self.collectionView?.reloadData()
    }) { (error) in
      print("failed to fetch ordered posts:", error)
    }
  }

  /// Get the users
  func fetchUser() {
    let uid = userId ?? Auth.auth().currentUser?.uid ?? ""
//    guard let uid = Auth.auth().currentUser?.uid else { return }
    Database.fetchUser(withUID: uid) { (user) in
      self.user = user
      self.navigationItem.title = self.user?.username
      self.collectionView?.reloadData()
      self.fetchOrderedPosts()
    }
  }
}

// MARK: View Setup
private extension UserProfileController {
  func setupLogoutButton() {
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "gear").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleLogout))
  }

  @objc func handleLogout() {
    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

    let logoutAction = UIAlertAction(title: "Log Out", style: .destructive) { (_) in
      print("Log out pressed")
      do {
        try Auth.auth().signOut()
        let loginController = LoginController()
        let navController = UINavigationController(rootViewController: loginController)
        self.present(navController, animated: true, completion: nil)

      } catch let signOutErr {
        print("Failed to sign out:", signOutErr)
      }
    }
    alertController.addAction(logoutAction)

    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    alertController.addAction(cancelAction)

    present(alertController, animated: true, completion: nil)
  }
}

// MARK: UserProfileHeaderDelegate
extension UserProfileController: UserProfileHeaderDelegate {
  func userProfileHeaderDidChangeToListView() {
    isGridView = false
    collectionView?.reloadData()
  }

  func userProfileHeaderDidChangeToGridView() {
    isGridView = true
    collectionView?.reloadData()
  }
}

// MARK: UICollectionViewDelegateFlowLayout
extension UserProfileController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
    return CGSize(width: view.frame.width, height: 200)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    // Subtract 2 to account for the inter item and minimum line spacing
    if isGridView {
      let width = (view.frame.width - 2) / 3
      return CGSize(width: width, height: width)
    } else {
      // ListView uses HomePostCell which is configured in HomeController

      var height: CGFloat = 40 + 16 // userProfileImageView + 8 padding on top and bottom
      height += view.frame.width
      height += 50 // controls  (favourite, message, share, etc.)
      height += 60 // captions

      return CGSize(width: view.frame.width, height: height)
    }
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
     return 1
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 1
  }
}
