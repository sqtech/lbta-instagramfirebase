//
//  UserProfilePhotoCell.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-03-05.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class UserProfilePhotoCell: UICollectionViewCell {
  var post: Post? {
    didSet {
//      print(post?.imageURL ?? "")
      guard let imageURL = post?.imageURL else { return }
      photoImageView.loadImage(urlString: imageURL)
    }
  }

  let photoImageView: CustomImageView = {
    let iv = CustomImageView()
    iv.contentMode = .scaleAspectFill
    iv.clipsToBounds = true
    return iv
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setupLayout()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

private extension UserProfilePhotoCell {
  func setupLayout() {
    addSubview(photoImageView)
    photoImageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
  }
}
