//
//  UserProfileHeader.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-02-10.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Firebase

protocol UserProfileHeaderDelegate: class {
  func userProfileHeaderDidChangeToListView()
  func userProfileHeaderDidChangeToGridView()
}

class UserProfileHeader: UICollectionViewCell {
  var user: AppUser? {
    didSet {
      setupProfileImage(from: user)
      usernameLabel.text = user?.username
      setupEditFollowButton()
    }
  }

  weak var delegate: UserProfileHeaderDelegate?

  private lazy var profileImageView: CustomImageView = {
    let iv = CustomImageView()
    iv.contentMode = .scaleAspectFit
    iv.isUserInteractionEnabled = true
    iv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleAddImageView)))
    return iv
  }()

  lazy var gridButton: UIButton = {
    let button = UIButton(type: .system)
    button.addTarget(self, action: #selector(handleChangeToGridView), for: .touchUpInside)
    button.setImage(#imageLiteral(resourceName: "grid"), for: .normal)
    button.tintColor = UIColor.lightBlue
    return button
  }()

  lazy var listButton: UIButton = {
    let button = UIButton(type: .system)
    button.addTarget(self, action: #selector(handleChangeToListView), for: .touchUpInside)
    button.setImage(#imageLiteral(resourceName: "list"), for: .normal)
    button.tintColor = UIColor(white: 0, alpha: 0.2)
    return button
  }()

  let bookmarkButton: UIButton = {
    let button = UIButton(type: .system)
    button.setImage(#imageLiteral(resourceName: "ribbon"), for: .normal)
    button.tintColor = UIColor(white: 0, alpha: 0.2)
    return button
  }()

  let usernameLabel: UILabel = {
    let label = UILabel()
    label.text = "Username"
    label.font = UIFont.boldSystemFont(ofSize: 14)
    return label
  }()

  let postsLabel: UILabel = {
    let label = UILabel()
//    label.text = "11\nposts"
    let attributedText = NSMutableAttributedString(string: "11\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
    attributedText.append(NSAttributedString(string: "posts", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)]))

    label.attributedText = attributedText

    label.numberOfLines = 0
    label.textAlignment = .center
    return label
  }()

  let followerLabel: UILabel = {
    let label = UILabel()
    let attributedText = NSMutableAttributedString(string: "0\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
    attributedText.append(NSAttributedString(string: "followers", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)]))

    label.attributedText = attributedText

    label.numberOfLines = 0
    label.textAlignment = .center
    return label
  }()

  let followingLabel: UILabel = {
    let label = UILabel()
    let attributedText = NSMutableAttributedString(string: "0\n", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
    attributedText.append(NSAttributedString(string: "following", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray, NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)]))

    label.attributedText = attributedText

    label.numberOfLines = 0
    label.textAlignment = .center
    return label
  }()

  lazy var editProfileFollowButton: UIButton = {
    let button = UIButton(type: .system)
    button.addTarget(self, action: #selector(handleEditProfileOrFollow), for: .touchUpInside)
    button.setTitle("Edit Profile", for: .normal)
    button.setTitleColor(.black, for: .normal)
    button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
    button.layer.borderColor = UIColor.lightGray.cgColor
    button.rounded()
    return button
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = .white

    setupLayout()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: Methods
private extension UserProfileHeader {
  func setupLayout() {
    addSubview(profileImageView)
    profileImageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 12, left: 12, bottom: 0, right: 0), size: .init(width: 80, height: 80))
    profileImageView.layer.cornerRadius = 80 / 2
    profileImageView.clipsToBounds = true

    addSubview(usernameLabel)

    setupBottomToolbar()

    usernameLabel.anchor(top: profileImageView.bottomAnchor, leading: profileImageView.leadingAnchor, bottom: gridButton.topAnchor, trailing: profileImageView.trailingAnchor, padding: .init(top: 4, left: 0, bottom: 0, right: 0))

    setupUserStatsView()
    addSubview(editProfileFollowButton)
    editProfileFollowButton.anchor(top: postsLabel.bottomAnchor, leading: postsLabel.leadingAnchor, bottom: nil, trailing: followingLabel.trailingAnchor, padding: .init(top: 2, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 34))

  }

  func setupUserStatsView() {
    let stackView = UIStackView(arrangedSubviews: [postsLabel, followerLabel, followingLabel])
    stackView.distribution = .fillEqually
    addSubview(stackView)
    stackView.anchor(top: profileImageView.topAnchor, leading: profileImageView.trailingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 12, bottom: 0, right: 12), size: .init(width: 0, height: 50))
  }

  func setupBottomToolbar() {
    let topDividerView = UIView()
    topDividerView.backgroundColor = UIColor.lightGray

    let bottomDividerView = UIView()
    bottomDividerView.backgroundColor = UIColor.lightGray

    let stackView = UIStackView(arrangedSubviews: [gridButton, listButton, bookmarkButton])
    stackView.axis = .horizontal
    stackView.distribution = .fillEqually

    addSubview(stackView)
    addSubview(topDividerView)
    addSubview(bottomDividerView)

    stackView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, size: .init(width: 0, height: 50))
    topDividerView.anchor(top: stackView.topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, size: .init(width: 0, height: 0.5))
    bottomDividerView.anchor(top: nil, leading: leadingAnchor, bottom: stackView.bottomAnchor, trailing: trailingAnchor, size: .init(width: 0, height: 0.5))
  }

  func setupProfileImage(from user: AppUser?) {
    if let profileImageURL = user?.profileImageURL {
      profileImageView.loadImage(urlString: profileImageURL)
    } else {
      profileImageView.image = #imageLiteral(resourceName: "plus_photo").withRenderingMode(.alwaysTemplate)
      profileImageView.tintColor = .gray
    }
  }

  func setupEditFollowButton() {
    guard let currentLoggedInUserId = Auth.auth().currentUser?.uid, let userId = user?.uid else { return }

    if currentLoggedInUserId == userId {

    } else {
      // Check if following
      Database.database().reference().child("following").child(currentLoggedInUserId).child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
        print(snapshot)
        // Can use this too
//        snapshot.exists()
        if let isFollowing = snapshot.value as? Int, isFollowing == 1 {
          self.editProfileFollowButton.setTitle("Unfollow", for: .normal)
        } else {
          self.setupFollowStyle()
        }
      }) { (err) in
        print("Failed to check if following: ", err)
      }

    }
  }

  func setupFollowStyle() {
    editProfileFollowButton.setTitle("Follow", for: .normal)
    editProfileFollowButton.setTitleColor(.white, for: .normal)
    editProfileFollowButton.backgroundColor = UIColor.lightBlue
    editProfileFollowButton.layer.borderColor = UIColor(white: 0, alpha: 0.2).cgColor
  }

  func setupUnfollowStyle() {
    editProfileFollowButton.setTitle("Unfollow", for: .normal)
    editProfileFollowButton.setTitleColor(.black, for: .normal)
    editProfileFollowButton.backgroundColor = .white
    editProfileFollowButton.layer.borderColor = UIColor(white: 0, alpha: 0.2).cgColor
  }

  // MARK: Handlers
  @objc func handleChangeToListView() {
    listButton.tintColor = .lightBlue
    gridButton.tintColor = UIColor(white: 0, alpha: 0.2)
    delegate?.userProfileHeaderDidChangeToListView()
  }

  @objc func handleChangeToGridView() {
    gridButton.tintColor = .lightBlue
    listButton.tintColor = UIColor(white: 0, alpha: 0.2)
    delegate?.userProfileHeaderDidChangeToGridView()
  }

  @objc func handleEditProfileOrFollow() {
    guard let currentLoggedInUserId = Auth.auth().currentUser?.uid, let userId = user?.uid else { return }

    if editProfileFollowButton.titleLabel?.text == "Unfollow" {
      // Unfollow
      Database.database().reference().child("following").child(currentLoggedInUserId).child(userId).removeValue { (err, ref) in
        if let err = err {
          print("Failed to unfollow user: ", err)
          return
        }
        print("Successfully unfollowed user: ", self.user?.username ?? "")
        self.setupFollowStyle()
      }
    } else {
      // Follow
      let ref = Database.database().reference().child("following").child(currentLoggedInUserId)
      let values = [userId: 1]
      ref.updateChildValues(values) { (err, ref) in
        if let err = err {
          print("Failed to follow user:", err)
          return
        }
        print("Successfully followed user: ", self.user?.username ?? "")
        self.setupUnfollowStyle()
      }

    }

  }

  @objc func handleAddImageView() {
    print("handleAddImageView")
  }
}
