//
//  CustomImageView.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-03-24.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

var imageCache = [String: UIImage]()

class CustomImageView: UIImageView {
  var lastURLUsedToLoadImage: String?

  func loadImage(urlString: String) {
    lastURLUsedToLoadImage = urlString

    self.image = nil

    if let cachedImage = imageCache[urlString] {
      self.image = cachedImage
      return
    }

    guard let url = URL(string: urlString) else { return }
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      if let error = error {
        print("Failed to fetch post image:", error)
        return
      }


      if url.absoluteString != self.lastURLUsedToLoadImage {
        print("URL not the same as last URL returning...")
        return
      }

      guard let imageData = data else { return }
      let photoImage = UIImage(data: imageData)
      imageCache[url.absoluteString] = photoImage

      DispatchQueue.main.async {
        self.image = photoImage

      }
    }.resume()
  }
}
