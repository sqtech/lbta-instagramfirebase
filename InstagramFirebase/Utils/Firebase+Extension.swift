//
//  Firebase+Extension.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-04-01.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Firebase

// MARK: Firebase Extension
extension Database {
  static func fetchUser(withUID uid: String, completion: @escaping (AppUser) -> Void) {
    print("Fetching user with uid:", uid)
    Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
      guard let userDictionary = snapshot.value as? [String: Any] else { return }
      let user = AppUser(uid: uid, dictionary: userDictionary)
      completion(user)
    }) { (err) in
      print("Failed to fetch user for posts:", err)
    }
  }
}
