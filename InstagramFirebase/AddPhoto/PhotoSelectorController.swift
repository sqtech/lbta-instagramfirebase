//
//  PhotoSelectorController.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-02-22.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Photos

class PhotoSelectorController: UICollectionViewController {
  struct CellIdentifiers {
    static let headerId = "HeaderId"
    static let cellId = "CellId"
  }

  var images = [UIImage]()
  var assets = [PHAsset]()
  var selectedImage: UIImage?
  var header: PhotoSelectorHeader? // get a reference to pass the header image to SharePhotoController later on

  override func viewDidLoad() {
    super.viewDidLoad()
    setupNavigationItems()

    // Keeps header at the top, ep15 look at comments to make it similar to real Instagram app
    let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
    layout?.sectionHeadersPinToVisibleBounds = true

    collectionView?.backgroundColor = .white
    collectionView?.register(PhotoSelectorHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: CellIdentifiers.headerId)
    collectionView?.register(PhotoSelectorCell.self, forCellWithReuseIdentifier: CellIdentifiers.cellId)

    fetchPhotos()
  }

  override var prefersStatusBarHidden: Bool {
    return true
  }

  // MARK: UICollectionViewDataSource
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return images.count
  }

  override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CellIdentifiers.headerId, for: indexPath) as! PhotoSelectorHeader

    self.header = header

    if let selectedImage = selectedImage {
      if let index = images.index(of: selectedImage) {
        let asset = assets[index]
        let imageManager = PHImageManager.default()
        let targetSize = CGSize(width: 600, height: 600)

        imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFill, options: nil) { (image, info) in
          header.photoImageView.image = image
        }
      }
    }

    return header
  }

  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.cellId, for: indexPath) as! PhotoSelectorCell
    cell.photoImageView.image = images[indexPath.row]
    return cell
  }

  // MARK: UICollectionViewDelegate
  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    selectedImage = images[indexPath.item]
    print(selectedImage ?? "")
    self.collectionView?.reloadData()

    // Scroll to top afterwards
    let indexPath = IndexPath(item: 0, section: 0)
    collectionView.scrollToItem(at: indexPath, at: .bottom, animated: true)
//    self.collectionView?.reloadSections(IndexSet(0..<1))
//    self.collectionView?.collectionViewLayout.invalidateLayout()
  }



}

// MARK: Methods
private extension PhotoSelectorController {
  func assetFetchOptions() -> PHFetchOptions {
    let fetchOptions = PHFetchOptions()
    fetchOptions.fetchLimit = 10
    let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
    fetchOptions.sortDescriptors = [sortDescriptor]
    return fetchOptions
  }

  func fetchPhotos() {
    let allPhotos = PHAsset.fetchAssets(with: .image, options: assetFetchOptions())

    DispatchQueue.global(qos: .background).async {
      allPhotos.enumerateObjects { (asset, count, stop) in
        let imageManager = PHImageManager.default()
        let targetSize = CGSize(width: 200, height: 200)
        let options = PHImageRequestOptions()
        // Prevents duplicates from being loaded
        options.isSynchronous = true
        imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFit, options: options, resultHandler: { (image, info) in
          if let image = image {
            self.images.append(image)
            self.assets.append(asset)
            if self.selectedImage == nil {
              self.selectedImage = image
            }
          }

          if count == allPhotos.count - 1 {
            DispatchQueue.main.async {
              self.collectionView?.reloadData()
            }
          }
        })
      }
    }
  }

  func setupNavigationItems() {
    setupCancelButton()
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(handleNext))
  }

  // MARK: Handlers
  @objc func handleNext() {
    print("Handle Next")
    // push
    let sharePhotoController = SharePhotoController()
    sharePhotoController.selectedImage = header?.photoImageView.image
    navigationController?.pushViewController(sharePhotoController, animated: true)

  }
}

// MARK: UICollectionViewDelegateFlowLayout
extension PhotoSelectorController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
    let width = view.frame.width
    return CGSize(width: width, height: width)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let width = (view.frame.width - 3) / 4
    return CGSize(width: width, height: width)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 1
  }
}
