//
//  SharePhotoController.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-03-04.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Firebase

class SharePhotoController: UIViewController {

  static let updateFeedNotificationName = NSNotification.Name(rawValue: "UpdateFeed")

  var selectedImage: UIImage? {
    didSet {
      imageView.image = selectedImage
    }
  }

  let imageView: UIImageView = {
    let iv = UIImageView()
    iv.backgroundColor = .red
    iv.clipsToBounds = true
    iv.contentMode = .scaleAspectFill
    return iv
  }()

  let textView: UITextView = {
    let tv = UITextView()
    tv.font = UIFont.systemFont(ofSize: 14)
    return tv
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
  }

  override var prefersStatusBarHidden: Bool {
    return true
  }
}

private extension SharePhotoController {
  @objc func handleShare() {
    guard let caption = textView.text else { return }
    if caption.isEmpty {
      showAlert(title: "Caption", message: "Please enter a caption")
      return
    }

    // Temporarily disable the share button to prevent user from pressing multiple times
    navigationItem.rightBarButtonItem?.isEnabled = false

    // Upload photo
    guard let image = selectedImage, let uploadData = UIImageJPEGRepresentation(image, 0.5) else { return }
    let filename = NSUUID().uuidString
    Storage.storage().reference(withPath: "posts").child(filename).putData(uploadData, metadata: nil) { (metaData, error) in
      if let error = error {
        print("Failed to upload post image:", error)
        return
      }

      guard let imageURL = metaData?.downloadURL()?.absoluteString else { return }
      self.saveToDatabase(with: imageURL, andCaption: caption, then: {_ in
        self.dismiss(animated: true, completion: nil)

        NotificationCenter.default.post(name: SharePhotoController.updateFeedNotificationName, object: nil)
      })

      print("Successfully uploaded post image", imageURL)
    }
  }

  func saveToDatabase(with imageURL: String, andCaption caption: String, then completion: @escaping (Error?) -> Void) {
    guard let postImage = selectedImage else { return }
    guard let uid = Auth.auth().currentUser?.uid else { return }

    // Go to the corresponding user id node
    let userPostRef = Database.database().reference(withPath: "posts").child(uid)
    // New node is auto generated everytime user posts something new
    let ref = userPostRef.childByAutoId()
    let values = [
      "imageURL": imageURL,
      "caption": caption,
      "imageWidth": postImage.size.width,
      "imageHeight": postImage.size.height,
      "creationDate": Date().timeIntervalSince1970
      ] as [String : Any]
    // Update the values
    ref.updateChildValues(values) { (error, ref) in
      if let error = error {
        print("Failed to save post to db:", error)
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        completion(error)
        return
      }

      print("Succesfully saved post to db")
      completion(nil)
    }
  }

  func setupLayout() {
    view.backgroundColor = UIColor.grayBackground

    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self, action: #selector(handleShare))
    
    let containerView = UIView()
    containerView.backgroundColor = .white

    view.addSubview(containerView)
    containerView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, size: .init(width: 0, height: 100))

    containerView.addSubview(imageView)
    imageView.anchor(top: containerView.topAnchor, leading: containerView.leadingAnchor, bottom: containerView.bottomAnchor, trailing: nil, padding: .init(top: 8, left: 8, bottom: 8, right: 8), size: .init(width: 84, height: 0)) // width is 84 because height will be 84, is calculated from anchoring to the top and bottom giving 100pt in height, minus padding from top (8) and bottom (8) == 16, 100-16 = 84 to make a perfect square

    containerView.addSubview(textView)
    textView.anchor(top: containerView.topAnchor, leading: imageView.trailingAnchor, bottom: containerView.bottomAnchor, trailing: containerView.trailingAnchor, padding: .init(top: 0, left: 4, bottom: 0, right: 0))
  }
}
