//
//  PhotoSelectorHeader.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-02-25.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class PhotoSelectorHeader: UICollectionViewCell {
  let photoImageView: UIImageView = {
    let iv = UIImageView()
    iv.contentMode = .scaleAspectFill
    iv.clipsToBounds = true
    iv.backgroundColor = .cyan
    return iv
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setupLayout()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}

private extension PhotoSelectorHeader {
  func setupLayout() {
    addSubview(photoImageView)
    photoImageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
  }
}
