//
//  Post.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-03-05.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Foundation

struct Post {
  var id: String?
  let caption: String
  let user: AppUser
  let imageURL: String
//  let imageHeight: Double
//  let imageWidth: Double
  let creationDate: Date
  var hasLiked: Bool = false


  init(user: AppUser, dictionary: [String: Any]) {
    self.user = user
    self.imageURL = dictionary["imageURL"] as? String ?? ""
    self.caption = dictionary["caption"] as? String ?? ""

    let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
//    Date(timeIntervalSinceNow: <#T##TimeInterval#>)
    self.creationDate = Date(timeIntervalSince1970: secondsFrom1970)
  }
}
