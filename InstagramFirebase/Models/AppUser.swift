//
//  AppUser.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-02-12.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Foundation

struct AppUser {
  let uid: String
  let profileImageURL: String
  let username: String

  init(uid: String, dictionary: [String: Any]) {
    self.uid = uid
    self.username = dictionary["username"] as? String ?? ""
    self.profileImageURL = dictionary["profileImageURL"] as? String ?? ""
  }
}
