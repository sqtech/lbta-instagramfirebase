//
//  Comment.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-05-15.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Foundation

struct Comment {
  let uid: String
  let text: String
  let user: AppUser
//  let creationDate: Date

  init(user: AppUser, dictionary: [String: Any]) {
    self.user = user
    self.uid = dictionary["uid"] as? String ?? ""
    self.text = dictionary["text"] as? String ?? ""
  }
}
