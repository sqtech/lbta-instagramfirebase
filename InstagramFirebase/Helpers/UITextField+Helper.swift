//
//  UITextField+Helper.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-01-27.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

extension UITextField {
//  let padding = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
  func setPaddingFor(left: CGFloat = 0, right: CGFloat = 0) {
    let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: left, height: self.frame.size.height))
    self.leftView = leftPaddingView
    self.leftViewMode = .always

    let rightPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: right, height: self.frame.size.height))
    self.rightView = rightPaddingView
    self.rightViewMode = .always
  }
}
