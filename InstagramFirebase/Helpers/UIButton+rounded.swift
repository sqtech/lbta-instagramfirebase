//
//  UIButton+rounded.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-02-04.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

extension UIButton {

  func rounded() {
    self.clipsToBounds = true
    self.layer.masksToBounds = true
    self.layer.cornerRadius = 4
    self.layer.borderWidth = 1
  }

  func circle() {
    self.clipsToBounds = true
    self.layer.masksToBounds = true
    self.layer.cornerRadius = self.frame.width / 2
    self.layer.borderWidth = 1
  }
}
