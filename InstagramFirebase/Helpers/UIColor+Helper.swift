//
//  UIColor+Helper.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-01-27.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

extension UIColor {
  convenience init(red: Int = 0, green: Int = 0, blue: Int = 0, opacity: Int = 255) {
    precondition(0...255 ~= red &&
      0...255 ~= green &&
      0...255 ~= blue &&
      0...255 ~= opacity, "Input out of range 0...255"
    )

    self.init(red: CGFloat(red)/255, green: CGFloat(green)/255, blue: CGFloat(blue)/255, alpha: CGFloat(opacity)/255)
  }

  static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
    return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
  }

  static let lightBlue = UIColor(red: 17/255, green: 154/255, blue: 237/255, alpha: 1)

  static let inactiveLightBlue = UIColor(red: 149/255, green: 204/255, blue: 244/255, alpha: 1)

  static let grayBackground = UIColor.rgb(red: 240, green: 240, blue: 240)
}
