//
//  UIViewController+Helper.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-02-22.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

extension UIViewController {
  func setupCancelButton() {
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
  }

  @objc func handleCancel() {
    dismiss(animated: true, completion: nil)
  }

  func showAlert(title: String, message: String, actionStyle: UIAlertActionStyle = .default, handler: ((UIAlertAction) -> Void)? = nil ) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: actionStyle, handler: handler)
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }
}
