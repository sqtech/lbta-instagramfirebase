//
//  Date+timeAgoDisplay.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-04-20.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import Foundation

extension Date {

  /// New way of formatting
  var relativelyFormatted: String {
    let now = Date()
    let components = Calendar.current.dateComponents([.year, .month, .weekOfYear, .day, .hour, .minute, .second], from: self, to: now) // from and to can be swapped based on how date was made: Date(timeIntervalSinceNow: ) vs Date(timeIntervalSince1970: )
//    print(Date(timeIntervalSinceNow: 3600))
//    print(Date(timeIntervalSince1970: 3600))

    if let years = components.year, years > 0 {
      return "\(years) year\(years == 1 ? "" : "s") ago"
    }
    if let months = components.month, months > 0 {
      return "\(months) month\(months == 1 ? "" : "s") ago"
    }

    if let months = components.month, months > 0 {
      return "\(months) month\(months == 1 ? "" : "s") ago"
    }
    if let weeks = components.weekOfYear, weeks > 0 {
      return "\(weeks) week\(weeks == 1 ? "" : "s") ago"
    }
    if let days = components.day, days > 0 {
      guard days > 1 else { return "yesterday" }

      return "\(days) day\(days == 1 ? "" : "s") ago"
    }

    if let hours = components.hour, hours > 0 {
      return "\(hours) hour\(hours == 1 ? "" : "s") ago"
    }

    if let minutes = components.minute, minutes > 0 {
      return "\(minutes) minute\(minutes == 1 ? "" : "s") ago"
    }

    if let seconds = components.second, seconds > 30 {
      return "\(seconds) second\(seconds == 1 ? "" : "s") ago"
    }

    return "just now"
  }

  func timeAgoDisplay() -> String {
    let secondsAgo = Int(Date().timeIntervalSince(self))
    let minute = 60
    let hour = 60 * minute
    let day = 24 * hour
    let week = 7 * day
    let month = 4 * week

    let quotient: Int
    let unit: String

    if secondsAgo < minute {
      quotient = secondsAgo
      unit = "second"
    } else if secondsAgo < hour {
      quotient = secondsAgo / minute
      unit = "min"
    } else if secondsAgo < day {
      quotient = secondsAgo / hour
      unit = "hour"
    } else if secondsAgo < week {
      quotient = secondsAgo / day
      unit = "day"
    } else if secondsAgo < month {
      quotient = secondsAgo / week
      unit = "week"
    } else {
      quotient = secondsAgo / month
      unit = "month"
    }


    return "\(quotient) \(unit)\(quotient == 1 ? "" : "s") ago"
  }
}
