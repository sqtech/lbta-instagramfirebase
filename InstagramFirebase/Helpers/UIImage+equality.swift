//
//  UIImage+equality.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-02-04.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

extension UIImage {
  // Figure out naming later
  func isEqualToImage(image: UIImage) -> Bool? {
    guard let data1 = UIImagePNGRepresentation(self), let data2 = UIImagePNGRepresentation(image) else { return nil }
    let imageData1 = data1 as NSData
    let imageData2 = data2 as NSData
    return imageData1.isEqual(imageData2)
  }

}
