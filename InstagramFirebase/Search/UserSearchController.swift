//
//  UserSearchController.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-04-01.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Firebase

class UserSearchController: UICollectionViewController {
  struct CellIdentifiers {
    static let searchCell = "SearchCell"
  }

  // Created after UserSearchController is instantiated, thus have access to self
  lazy var searchBar: UISearchBar = {
    let sb = UISearchBar()
    sb.placeholder = "Enter username"
    sb.barTintColor = .gray
    sb.delegate = self
    UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor.rgb(red: 240, green: 240, blue: 240)
    return sb
  }()

  var users = [AppUser]()
  var filteredUsers = [AppUser]()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
    fetchUsers()
  }

  // MARK: UICollectionViewDataSource
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return filteredUsers.count
  }

  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.searchCell, for: indexPath) as! UserSearchCell
    cell.user = filteredUsers[indexPath.item]
    return cell
  }
}

// MARK: UICollectionViewDelegate
extension UserSearchController {
  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    searchBar.resignFirstResponder()
    let user = filteredUsers[indexPath.item]
    let userProfileController = UserProfileController(collectionViewLayout: UICollectionViewFlowLayout())
    userProfileController.userId = user.uid
    navigationController?.pushViewController(userProfileController, animated: true)
  }
}

// MARK: UICollectionViewDelegateFlowLayout
extension UserSearchController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: view.frame.width, height: 66)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
}

// MARK: UISearchBarDelegate
extension UserSearchController: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    if searchText.isEmpty {
      filteredUsers = users
    } else {
      filteredUsers = users.filter { (user) -> Bool in
        return user.username.lowercased().contains(searchText.lowercased())
      }
    }

    self.collectionView?.reloadData()
  }
}

private extension UserSearchController {
  func setupLayout() {
    navigationItem.titleView = searchBar
    collectionView?.backgroundColor = .white
    collectionView?.alwaysBounceVertical = true
    collectionView?.keyboardDismissMode = .onDrag
    collectionView?.register(UserSearchCell.self, forCellWithReuseIdentifier: CellIdentifiers.searchCell)
  }

  func fetchUsers() {
    let ref = Database.database().reference().child("users")
    ref.observeSingleEvent(of: .value, with: { (snapshot) in
      guard let dictionaries = snapshot.value as? [String: Any] else { return }
      dictionaries.forEach({ (key, value) in

        // Remove yourself from the search list
        if key == Auth.auth().currentUser?.uid {
          return
        }

        guard let userDictionary = value as? [String: Any] else { return }
        let user = AppUser(uid: key, dictionary: userDictionary)
        self.users.append(user)
//        print(user.uid, user.username)
      })
      self.users.sort(by: { $0.username.compare($1.username) == .orderedAscending })
      self.filteredUsers = self.users
      self.collectionView?.reloadData()

    }) { (err) in
      print("Failed to fetch users for search:", err)
    }
  }
}
