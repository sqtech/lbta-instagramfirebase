//
//  UserSearchCell.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-04-01.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class UserSearchCell: UICollectionViewCell {
  var user: AppUser? {
    didSet {
      usernameLabel.text = user?.username

      guard let profileImageURL = user?.profileImageURL else { return }
      profileImageView.loadImage(urlString: profileImageURL)
    }
  }
  
  let profileImageView: CustomImageView = {
    let iv = CustomImageView()
    iv.contentMode = .scaleAspectFill
    iv.clipsToBounds = true
    return iv
  }()

  let usernameLabel: UILabel = {
    let label = UILabel()
    label.text = "Username"
    label.font = UIFont.boldSystemFont(ofSize: 14)
    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setupLayout()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
} 

extension UserSearchCell {
  func setupLayout() {
    backgroundColor = .white
    addSubview(profileImageView)
    addSubview(usernameLabel)

    profileImageView.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 0, left: 8, bottom: 0, right: 0), size: .init(width: 50, height: 50))
    profileImageView.layer.cornerRadius = 50 / 2
    profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

    usernameLabel.anchor(top: topAnchor, leading: profileImageView.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 8, bottom: 0, right: 0))
    setupSeparatorLine()
  }

  /// Adds a separator line between each cell
  func setupSeparatorLine() {
    let separatorView = UIView()
    separatorView.backgroundColor = UIColor(white: 0, alpha: 0.5)
    addSubview(separatorView)
    separatorView.anchor(top: nil, leading: usernameLabel.leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, size: .init(width: 0, height: 0.5))
  }
}
