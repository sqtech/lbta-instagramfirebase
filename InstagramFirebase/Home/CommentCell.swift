//
//  CommentCell.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-05-15.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

class CommentCell: UICollectionViewCell {
  var comment: Comment? {
    didSet {
      guard
        let comment = comment
      else { return }

      profileImageView.loadImage(urlString: comment.user.profileImageURL)

      let attributedText = NSMutableAttributedString(string: comment.user.username, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
      attributedText.append(NSAttributedString(string: " " + comment.text, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
      textView.attributedText = attributedText
    }
  }

  let profileImageView: CustomImageView = {
    let iv = CustomImageView()
    iv.clipsToBounds = true
    iv.contentMode = .scaleAspectFill
    iv.backgroundColor = .blue
    iv.layer.cornerRadius = 40 / 2 // 40 is wrt to the height that is set
    return iv
  }()

  // UITextView will render text from very top instead of center like a UILabel
  let textView: UITextView = {
    let textView = UITextView()
    textView.font = UIFont.systemFont(ofSize: 14)
    textView.isEditable = false

    // Must set this to false otherwise inside collectionView(_:,layout:, sizeForItemAt:) dummyCell estimatedSize will be full height of screen
    textView.isScrollEnabled = false
//    label.numberOfLines = 0
    return textView
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setupLayout()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

private extension CommentCell {
  func setupLayout() {
    backgroundColor = .white
    addSubview(profileImageView)
    profileImageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 8, left: 8, bottom: 0, right: 0), size: .init(width: 40, height: 40))

    addSubview(textView)
    textView.anchor(top: topAnchor, leading: profileImageView.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 4, bottom: 0, right: 4))
  }
}
