//
//  CommentsController.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-05-12.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Firebase

class CommentsController: UICollectionViewController {

  struct CellIdentifiers {
    static let commentCell = "CommentCell"
  }
  lazy var comments = [Comment]()
  var post: Post?

  /// containerView will now have a reference object (this class) for inputAcessory
  /// see ep34 LBTA
  /// not in context of CommentsController when creating containerView (with just var)
  /// thus can not access commentTextField
  /// need to use lazy var
  lazy var containerView: UIView = {
    let containerView = UIView()
    containerView.backgroundColor = .white
    containerView.frame = CGRect(x: 0, y: 0, width: 0, height: 50)

    let submitButton = UIButton(type: .system)
    submitButton.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
    submitButton.setTitle("Submit", for: .normal)
    submitButton.setTitleColor(.black, for: .normal)
    submitButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
    containerView.addSubview(submitButton)

    submitButton.anchor(top: containerView.topAnchor, leading: nil, bottom: containerView.bottomAnchor, trailing: containerView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 12), size: .init(width: 50, height: 0))

    containerView.addSubview(commentTextField)
    commentTextField.anchor(top: containerView.topAnchor, leading: containerView.leadingAnchor, bottom: containerView.bottomAnchor, trailing: submitButton.leadingAnchor, padding: .init(top: 0, left: 12, bottom: 0, right: 0))

    let lineSeparatorView = UIView()
    lineSeparatorView.backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230)
    containerView.addSubview(lineSeparatorView)
    lineSeparatorView.anchor(top: containerView.topAnchor, leading: containerView.leadingAnchor, bottom: nil, trailing: containerView.trailingAnchor, size: .init(width: 0, height: 1))

    return containerView
  }()

  let commentTextField: UITextField = {
    let tf = UITextField()
    tf.placeholder = "Enter Comment"
    return tf
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
    fetchComments()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    tabBarController?.tabBar.isHidden = true
  }
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    tabBarController?.tabBar.isHidden = false
  }


  override var inputAccessoryView: UIView? {
    get {
      return containerView
    }
  }

  override var canBecomeFirstResponder: Bool {
    return true
  }
}

// MARK: Private methods
private extension CommentsController {
  func fetchComments() {
    guard let postId = post?.id else { return }
    let ref = Database.database().reference(withPath: "comments").child(postId)

    ref.observe(.childAdded, with: { snapshot in
      guard
        let dictionary = snapshot.value as? [String: Any],
        let uid = dictionary["uid"] as? String
      else { return }

      Database.fetchUser(withUID: uid) { user in
        let comment = Comment(user: user, dictionary: dictionary)
        self.comments.append(comment)
        self.collectionView?.reloadData()
      }

    }) { err in
      print("Failed to observe comments:", err)
    }
  }

  func setupLayout() {
    navigationItem.title = "Comments"
    collectionView?.backgroundColor = .white
    collectionView?.alwaysBounceVertical = true
    collectionView?.keyboardDismissMode = .interactive
    
    collectionView?.register(CommentCell.self, forCellWithReuseIdentifier: CellIdentifiers.commentCell)


//    collectionView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -50, right: 0)
  }
}

// MARK: Handlers
private extension CommentsController {
  @objc func handleSubmit() {
    guard
      let uid = Auth.auth().currentUser?.uid,
      let postId = post?.id,
      let text = commentTextField.text, !text.isEmpty
      else { return }

    let values = ["text": text, "creationDate": Date().timeIntervalSince1970, "uid": uid] as [String : Any]
    Database.database().reference().child("comments").child(postId).childByAutoId().updateChildValues(values) { (err, ref) in
      if let err = err {
        print("Failed to insert comment:", err)
        return
      }

      print("Successfully inserted comment:", self.commentTextField.text ?? "")
    }

  }
}

// MARK: UICollectionViewDataSource
extension CommentsController {
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return comments.count
  }

  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.commentCell, for: indexPath) as! CommentCell

    cell.comment = comments[indexPath.item]
    return cell
  }
}

extension CommentsController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
    let dummyCell = CommentCell(frame: frame)
    dummyCell.comment = comments[indexPath.item]

    // call this because we just updated the cell with a new comment object
    dummyCell.layoutIfNeeded()

    let targetSize = CGSize(width: view.frame.width, height: 1000)
    let estimatedSize = dummyCell.systemLayoutSizeFitting(targetSize)
    // 16 is padding for top and bottom for the imageView
    let height = max(40 + 16, estimatedSize.height)
    return CGSize(width: view.frame.width, height: height)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
}

