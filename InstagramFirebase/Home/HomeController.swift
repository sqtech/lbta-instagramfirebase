//
//  HomeController.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-03-24.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit
import Firebase

class HomeController: UICollectionViewController {
  struct CellIdentifiers {
    static let headerId = "HeaderId"
    static let cellId = "CellId"
  }

  var posts = [Post]()

  override func viewDidLoad() {
    super.viewDidLoad()
    fetchPosts()
    setupLayout()
  }

  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return posts.count
  }

  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.cellId, for: indexPath) as! HomePostCell
    if indexPath.item < posts.count {
      cell.delegate = self
      cell.post = posts[indexPath.item]
    }
    return cell
  }
}

// MARK: Firebase API Method Calls
private extension HomeController {
  func fetchPosts() {
//    print("fetchPosts")
    guard let uid = Auth.auth().currentUser?.uid else { return }

    Database.database().reference().child("following").child(uid).observeSingleEvent(of: .value, with: { snapshot in
//      print(snapshot.value ?? "")
      guard let userIdsDictionary = snapshot.value as? [String: Any] else { return }
      userIdsDictionary.forEach({ key, value in
        Database.fetchUser(withUID: key) { user in
          self.fetchPosts(with: user)
        }
      })

    }) { error in
      print("Failed to fetch user ids:", error)
    }

    // Was fetching only the current logged in user posts
    Database.fetchUser(withUID: uid) { user in
      self.fetchPosts(with: user)
    }

  }

  func fetchPosts(with user: AppUser ) {
    let ref = Database.database().reference(withPath: "posts").child(user.uid)
    ref.observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
      //        print("HomeController Snapshot:", snapshot.key)
      self.collectionView?.refreshControl?.endRefreshing()

      guard let dictionaries = snapshot.value as? [String: Any] else { return }

      dictionaries.forEach({ (key, value) in
        guard let dictionary = value as? [String: Any] else { return }

        var post = Post(user: user, dictionary: dictionary)
        post.id = key

        guard let uid = Auth.auth().currentUser?.uid else { return }
        // child of likes is the post id
        Database.database().reference(withPath: "likes").child(key).child(uid).observeSingleEvent(of: .value, with: { snapshot in
          if let value = snapshot.value as? Int, value == 1 {
            post.hasLiked = true
          } else {
            post.hasLiked = false
          }

          self.posts.append(post)
          // Sort by latest, may be better to sort after all fetches are done?
          self.posts.sort { $0.creationDate.compare($1.creationDate) == .orderedDescending }
          self.collectionView?.reloadData()

        }, withCancel: { err in
          print("Failed to fetch like info for post: ", err)
        })

      })


    }) { (error) in
      print("Failed to fetch user posts:", error)
    }
  }

}

// MARK: Private Methods
private extension HomeController {
  func setupNavigationItems() {
    navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "logo2"))
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "camera3").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleCamera))
  }

  func setupLayout() {
    collectionView?.backgroundColor = .white
    collectionView?.register(HomePostCell.self, forCellWithReuseIdentifier: CellIdentifiers.cellId)

    let refreshControl = UIRefreshControl()
    refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
    collectionView?.refreshControl = refreshControl

    NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: SharePhotoController.updateFeedNotificationName, object: nil)
    setupNavigationItems()
  }

  // MARK: Handlers
  @objc func handleCamera() {
    print("Show camera")
    let cameraController = CameraController()
    present(cameraController, animated: true, completion: nil)
  }

  @objc func handleUpdateFeed() {
    handleRefresh()
  }

  @objc func handleRefresh() {
    posts.removeAll()
    fetchPosts()
  }
}

// MARK: UICollectionViewDelegateFlowLayout
extension HomeController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    var height: CGFloat = 40 + 16 // userProfileImageView + 8 padding on top and bottom
    height += view.frame.width
    height += 50 // controls  (favourite, message, share, etc.)
    height += 60 // captions

    return CGSize(width: view.frame.width, height: height)
  }
}

// MARK: HomePostCellDelegate
extension HomeController: HomePostCellDelegate {
  func homePostCell(_ cell: HomePostCell, didTapCommentFor post: Post) {
    let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
    commentsController.post = post
    navigationController?.pushViewController(commentsController, animated: true)
  }

  func homePostCell(_ cell: HomePostCell, didTapLikeFor post: Post) {
    guard
      let uid = Auth.auth().currentUser?.uid,
      let postId = post.id,
      let indexPath = collectionView?.indexPath(for: cell)
    else { return }

    /// This is the same post as the post passed from the parameter. However, Swift says that the value is a let and can not edit it...
    var postToEdit = posts[indexPath.item]

    /// If they have liked it, we unlike it (0), else otherwise we like it (1)
    let values = [uid: postToEdit.hasLiked ? 0 : 1]
    Database.database().reference(withPath: "likes").child(postId).updateChildValues(values) { (err, ref) in
      if let err = err {
        print("Failed to like post", err)
        return
      }

      print("Successfully liked post with caption:", post.caption)
      postToEdit.hasLiked = !post.hasLiked
      self.posts[indexPath.item] = postToEdit
      self.collectionView?.reloadItems(at: [indexPath])
    }
  }


}
