//
//  HomePostCell.swift
//  InstagramFirebase
//
//  Created by Simon Quach on 2018-03-25.
//  Copyright © 2018 Simon Quach. All rights reserved.
//

import UIKit

protocol HomePostCellDelegate: class {
  func homePostCell(_ cell: HomePostCell, didTapCommentFor post: Post)
  func homePostCell(_ cell: HomePostCell, didTapLikeFor post: Post)
}

class HomePostCell: UICollectionViewCell {
//  private let dateFormatter: DateComponentsFormatter = {
//    let formatter = DateComponentsFormatter()
//    formatter.unitsStyle = .full
//    formatter.allowedUnits = [.year,.day,.hour,.minute, .second]
////    formatter.string(from: 1234567900)
////    formatter.dateStyle = .medium
////    formatter.timeStyle = .short
//    return formatter
//  }()

  var delegate: HomePostCellDelegate?

  var post: Post? {
    didSet {
      guard let postImageURL = post?.imageURL else { return }
      photoImageView.loadImage(urlString: postImageURL)
      usernameLabel.text = post?.user.username
      likeButton.setImage(post?.hasLiked == true ? #imageLiteral(resourceName: "like_selected").withRenderingMode(.alwaysOriginal) : #imageLiteral(resourceName: "like_unselected").withRenderingMode(.alwaysOriginal), for: .normal)

      guard let profileImageURL = post?.user.profileImageURL else { return }
      userProfileImageView.loadImage(urlString: profileImageURL)
      setupAttributedCaption()
    }
  }

  let userProfileImageView: CustomImageView = {
    let iv = CustomImageView()
    iv.contentMode = .scaleAspectFill
    iv.clipsToBounds = true
    iv.backgroundColor = .blue
    return iv
  }()

  let photoImageView: CustomImageView = {
    let iv = CustomImageView()
    iv.contentMode = .scaleAspectFill
    iv.clipsToBounds = true
    return iv
  }()

  let usernameLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.boldSystemFont(ofSize: 14)
    return label
  }()

  let optionsButton: UIButton = {
    let button = UIButton(type: .system)
    button.setTitle("•••", for: .normal)
    button.setTitleColor(.black, for: .normal)
    return button
  }()

  lazy var likeButton: UIButton = {
    let button = UIButton(type: .system)
    button.addTarget(self, action: #selector(handleLike), for: .touchUpInside)
    button.setImage(#imageLiteral(resourceName: "like_unselected").withRenderingMode(.alwaysOriginal), for: .normal)
    return button
  }()

  lazy var commentButton: UIButton = {
    let button = UIButton(type: .system)
    button.addTarget(self, action: #selector(handleComment), for: .touchUpInside)
    button.setImage(#imageLiteral(resourceName: "comment").withRenderingMode(.alwaysOriginal), for: .normal)
    return button
  }()

  let sendMessageButton: UIButton = {
    let button = UIButton(type: .system)
    button.setImage(#imageLiteral(resourceName: "send2").withRenderingMode(.alwaysOriginal), for: .normal)
    return button
  }()

  let bookmarkButton: UIButton = {
    let button = UIButton(type: .system)
    button.setImage(#imageLiteral(resourceName: "ribbon").withRenderingMode(.alwaysOriginal), for: .normal)
    return button
  }()

  let captionLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    return label
  }()


  override init(frame: CGRect) {
    super.init(frame: frame)
    setupLayout()
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: Private handlers
private extension HomePostCell {
  @objc func handleLike() {
    guard let post = post else { return }
    delegate?.homePostCell(self, didTapLikeFor: post)
  }

  @objc func handleComment() {
    guard let post = post else { return }
    delegate?.homePostCell(self, didTapCommentFor: post)
  }
}

// MARK: Private methods
private extension HomePostCell {
  func setupAttributedCaption() {
    guard let post = self.post else { return }

    let attributedText = NSMutableAttributedString(string: post.user.username, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
    attributedText.append(NSAttributedString(string: " \(post.caption)", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))

    // Add some gap between the labels
    attributedText.append(NSAttributedString(string: "\n\n", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 4)]))

//    let timeAgoDisplay = post.creationDate.timeAgoDisplay()
//    guard let timeAgoDisplay = dateFormatter.string(from: post.creationDate.timeIntervalSinceNow) else { return }
    let timeAgoDisplay = post.creationDate.relativelyFormatted
    attributedText.append(NSAttributedString(string: timeAgoDisplay, attributes: [NSAttributedStringKey.font: UIFont.italicSystemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor.gray]))

    captionLabel.attributedText = attributedText
  }

  func setupActionButtons() {
    let stackView = UIStackView(arrangedSubviews: [likeButton, commentButton, sendMessageButton])
    stackView.axis = .horizontal
    stackView.distribution = .fillEqually
    addSubview(stackView)
    stackView.anchor(top: photoImageView.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 0, left: 8, bottom: 0, right: 0), size: .init(width: 132, height: 50))

    addSubview(bookmarkButton)
    bookmarkButton.anchor(top: stackView.topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 44, height: 50))
  }

  func setupLayout() {
    backgroundColor = .white

    addSubview(userProfileImageView)
    addSubview(usernameLabel)
    addSubview(optionsButton)
    addSubview(photoImageView)

    userProfileImageView.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 8, left: 8, bottom: 0, right: 0), size: .init(width: 40, height: 40))
    userProfileImageView.layer.cornerRadius = 40 / 2

    usernameLabel.anchor(top: nil, leading: userProfileImageView.trailingAnchor, bottom: nil, trailing: optionsButton.leadingAnchor, padding: .init(top: 0, left: 8, bottom: 0, right: 8))
    usernameLabel.centerYAnchor.constraint(equalTo: userProfileImageView.centerYAnchor).isActive = true

    optionsButton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 8), size: .init(width: 44, height: 0))
    optionsButton.centerYAnchor.constraint(equalTo: userProfileImageView.centerYAnchor).isActive = true

    photoImageView.anchor(top: userProfileImageView.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 8, left: 0, bottom: 0, right: 0))
    photoImageView.heightAnchor.constraint(equalTo: widthAnchor, multiplier: 1).isActive = true

    setupActionButtons()

    addSubview(captionLabel)
    captionLabel.anchor(top: likeButton.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 0, left: 8, bottom: 0, right: 8))

  }
}
